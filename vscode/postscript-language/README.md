# PostScript Language for Visual Studio Code

This extension provides syntax highlighting, bracket matching in PostScript files ported from TextMate.

## Release Notes

Users appreciate release notes as you update your extension.

### 0.0.1

Initial release.

## License

This package provides content based on the original work(s) below:

**TextMate support for Postscript** License

> If not otherwise specified (see below), files in this repository fall under the following license:
>
> 	  Permission to copy, use, modify, sell and distribute this
> 	  software is granted. This software is provided "as is" without
> 	  express or implied warranty, and with no claim as to its
> 	  suitability for any purpose.
>
> An exception is made for files in readable text which contain their own license information, or files where an accompanying file exists (in the same directory) with a “-license” suffix added to the base-name name of the original file, and an extension of txt, html, or similar. For example “tidy” is accompanied by “tidy-license.txt”.
>
> **Source**: [Original Repository](https://github.com/textmate/postscript.tmbundle)
