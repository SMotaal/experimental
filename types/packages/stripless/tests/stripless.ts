import type from 'stripless/types';
import * as stripless from 'stripless';

interface __exports__ {
  default: typeof stripless.default,
  functionᵗʸᵖᵉ: typeof stripless.functionᵗʸᵖᵉ,
  objectᵗʸᵖᵉ: typeof stripless.objectᵗʸᵖᵉ,
  stringᵗʸᵖᵉ: typeof stripless.stringᵗʸᵖᵉ,
  numberᵗʸᵖᵉ: typeof stripless.numberᵗʸᵖᵉ,
  bigintᵗʸᵖᵉ: typeof stripless.bigintᵗʸᵖᵉ,
  booleanᵗʸᵖᵉ: typeof stripless.booleanᵗʸᵖᵉ,
  symbolᵗʸᵖᵉ: typeof stripless.symbolᵗʸᵖᵉ,
  anyᵗʸᵖᵉ: typeof stripless.anyᵗʸᵖᵉ,
  unknownᵗʸᵖᵉ: typeof stripless.unknownᵗʸᵖᵉ,
}
