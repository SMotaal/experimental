/// types.js
const type = {
  /**
   * @template {*} T
   * @param {...T} values
   * @returns {T}
   */
  of(...values) {},
};

/** @type {Function} */
export const functionᵗʸᵖᵉ = (type.function = undefined);
/** @type {{}} */
export const objectᵗʸᵖᵉ = (type.object = undefined);
/** @type {string} */
export const stringᵗʸᵖᵉ = (type.string = undefined);
/** @type {number} */
export const numberᵗʸᵖᵉ = (type.number = undefined);
/** @type {bigint} */
export const bigintᵗʸᵖᵉ = (type.bigint = undefined);
/** @type {boolean} */
export const booleanᵗʸᵖᵉ = (type.boolean = undefined);
/** @type {symbol} */
export const symbolᵗʸᵖᵉ = (type.symbol = undefined);
/** @type {any} */
export const anyᵗʸᵖᵉ = (type.any = undefined);
/** @type {unknown} */
export const unknownᵗʸᵖᵉ = (type.unknown = undefined);

export default type;
