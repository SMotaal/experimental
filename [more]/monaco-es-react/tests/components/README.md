# Components

## Performance

- [What forces layout / reflow](https://gist.github.com/paulirish/5d52fb081b3570c81e3a)
- [FastDOM](https://github.com/wilsonpage/fastdom)
- [CodeLabs](https://codelabs.developers.google.com/io2018)
- [Memory Leaks](https://auth0.com/blog/four-types-of-leaks-in-your-javascript-code-and-how-to-get-rid-of-them/)
