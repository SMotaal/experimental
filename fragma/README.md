# <code>ƒragma</code>

Declarative HTML templating for Web Components.

## Proof of Concept

_Throttle_ shows the untapped potential of the DOM.

<pre>alpha/throttle.html#<samp>n</samp></pre>

| <samp>n</samp>    |   mobile   |  desktop   |
| ----------------- | :--------: | :--------: |
| [×100][~100]      |    most    |            |
| [×1,000][~1000]   |    many    |    most    |
| [×2,000][~2000]   |   latest   |    many    |
| [×5,000][~5000]   |  high-end  |   latest   |
| [×10,000][~10000] | (overkill) |  high-end  |
| [×20,000][~20000] |            | (overkill) |

[More details](./alpha/README.md)

[~100]: ./alpha/throttle.html#100
[~1000]: ./alpha/throttle.html#1000
[~2000]: ./alpha/throttle.html#2000
[~5000]: ./alpha/throttle.html#5000
[~10000]: ./alpha/throttle.html#10000
[~20000]: ./alpha/throttle.html#20000
